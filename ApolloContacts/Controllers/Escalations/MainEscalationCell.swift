//
//  MainEscalationCell.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class MainEscalationCell: UITableViewCell {
    
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureView(escalationContact: Escalation.Contact){
        contactName.text = escalationContact.name
        phone.text = escalationContact.phone
        position.text = escalationContact.position
    }
    
}
