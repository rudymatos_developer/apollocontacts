//
//  EscalationsVC.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class EscalationsVC: UIViewController {
    
    private let firebaseService = FirebaseService()
    private var escalations = [Escalation]()
    @IBOutlet weak var escalationsTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadEscalations()
    }


    private func loadEscalations(){
        
        firebaseService.getAll(type: Escalation.self) { (escalations) in
            if let escalations = escalations{
                self.escalations = escalations
                DispatchQueue.main.async {
                    self.escalationsTV.reloadData()
                }
            }
        }
    }
    
}

extension EscalationsVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row <= escalations[indexPath.section].contacts.count - 1{
            return 80
        }else if indexPath.row <= escalations[indexPath.section].contacts.count - 1{
            return 50
        }else{
            return 10
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return escalations.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return escalations[section].contacts.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row <= escalations[indexPath.section].contacts.count - 1{
            let selectedEscalation = escalations[indexPath.section].contacts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainEscalationCell", for: indexPath) as! MainEscalationCell
            cell.configureView(escalationContact: selectedEscalation)
            return cell
        }else if indexPath.row <= escalations[indexPath.section].contacts.count - 1{
            let selectedEscalation = escalations[indexPath.section].contacts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "escalationContactCell", for: indexPath) as! EscalationContactCell
            cell.configureView(escalationContact: selectedEscalation)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "separatorCell", for: indexPath) as! SeparatorCell
            return cell
        }
    }
    
}
