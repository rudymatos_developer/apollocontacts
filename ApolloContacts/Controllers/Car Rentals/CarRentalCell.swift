//
//  CarRentalCell.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class CarRentalCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var accountNumber: UILabel!
    @IBOutlet weak var hotl: UILabel!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var phoneContacts: UILabel!
    @IBOutlet weak var mailContacts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureView(carRental: CarRental){
        self.name.text = carRental.name
        self.accountNumber.text = "Account Number: \(carRental.accountNumber ?? "NA")"
        self.hotl.text = "Hotl: \(carRental.hotl ?? "NA")"
        self.notes.text = "Notes: \(carRental.notes ?? "NA")"
        self.phoneContacts.text = "Phones: \(carRental.contactPhones?.joined(separator: ",") ?? "NA")"
         self.mailContacts.text = "Emails: \(carRental.contactEmails?.joined(separator: ",") ?? "NA")"
    }

}
