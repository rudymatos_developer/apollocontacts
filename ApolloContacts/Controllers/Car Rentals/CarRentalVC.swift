//
//  CarRentalVC.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class CarRentalVC: UIViewController {

    private let firebaseService = FirebaseService()
    @IBOutlet weak var carRentalTV: UITableView!
    var carRentals = [CarRental]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView(){
        firebaseService.getAll(type: CarRental.self) { (carRentals) in
            guard let carRentals = carRentals else {return}
            self.carRentals = carRentals.sorted(by: {$0.name < $1.name})
            DispatchQueue.main.async {
                self.carRentalTV.reloadData()
            }
        }
    }

}

extension CarRentalVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carRentals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carRentalCell", for: indexPath) as! CarRentalCell
        let selectedCarRental = carRentals[indexPath.row]
        cell.configureView(carRental: selectedCarRental)
        return cell
    }
    
}
