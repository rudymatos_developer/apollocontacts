//
//  TeamInfoCell.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class TeamInfoCell: UITableViewCell {

    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamNotes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureView(teamName: String, teamNotes: String){
        self.teamName.text = teamName
        self.teamNotes.text = teamNotes
    }
    
}
