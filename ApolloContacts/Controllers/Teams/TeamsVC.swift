//
//  TeamsVC.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class TeamsVC: UIViewController {

    @IBOutlet weak var teamsTV: UITableView!
    private let firebaseService = FirebaseService()
    fileprivate var teams = [Team]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTeams()
    }

    private func loadTeams(){
        firebaseService.getAll(type: Team.self, completion: { (teams) in
            if let teams = teams{
                self.teams = teams
                DispatchQueue.main.async {
                    self.teamsTV.reloadData()
                }
            }
        })
    }
}

extension TeamsVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 150 : 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams[section].contacts.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let selectedTeam = teams[indexPath.section]
            let cell  = tableView.dequeueReusableCell(withIdentifier: "teamInfoCell", for: indexPath) as! TeamInfoCell
            cell.configureView(teamName: selectedTeam.name, teamNotes: selectedTeam.notes)
            return cell
        }else{
            let cell  = tableView.dequeueReusableCell(withIdentifier: "teamContactCell", for: indexPath) as! TeamContactCell
            let selectedContactName = teams[indexPath.section].contacts[indexPath.row - 1]
            cell.configureView(contactName: selectedContactName)
            return cell
        }
    }
}
