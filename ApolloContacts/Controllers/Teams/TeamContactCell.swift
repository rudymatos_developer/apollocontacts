//
//  TeamContactCell.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class TeamContactCell: UITableViewCell {

    @IBOutlet weak var contactName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureView(contactName : String){
        self.contactName.text = contactName
    }

}
