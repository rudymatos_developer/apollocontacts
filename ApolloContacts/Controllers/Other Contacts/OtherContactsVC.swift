//
//  ViewController.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class OtherContactsVC: UIViewController {

    fileprivate let firebaseService = FirebaseService()
    fileprivate var contacts = [OtherContacts]()
    fileprivate var filteredContacts = [OtherContacts]()
    @IBOutlet weak var contactTV: UITableView!
    fileprivate var selectedContact : OtherContacts?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        loadData()
    }
    
    
    private func configureView(){
        
        let searchBar = UISearchController(searchResultsController: nil)
        searchBar.searchBar.delegate = self
        searchBar.searchBar.placeholder = "Search Contact"
        searchBar.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        self.navigationItem.searchController = searchBar
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
    }

    fileprivate func loadData(){
        firebaseService.getAll(type: OtherContacts.self, completion: { (otherContacts) in
            guard let otherContacts = otherContacts else{
                return
            }
            self.contacts = otherContacts.sorted(by: {$0.contactName < $1.contactName})
            self.filteredContacts = self.contacts
            DispatchQueue.main.async {
                self.contactTV.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ContactInfoVC
        destination.delegate = self
        if segue.identifier == "showContactInfoSegue" {
            destination.contactInfoMode = .update
            destination.otherContact = selectedContact
        }
    }
    
}

extension OtherContactsVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let text = searchBar.text, text.trimmingCharacters(in: .whitespaces) != ""{
           filteredContacts = contacts.filter({$0.contactName.contains(text)})
        }else{
            filteredContacts = contacts
        }
        DispatchQueue.main.async {
            self.contactTV.reloadData()
        }
    }
}

extension OtherContactsVC: ContactInfoChangerDelegate{
    
    func updateData() {
        loadData()
    }
    
}


extension OtherContactsVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedContact = filteredContacts[indexPath.row]
        performSegue(withIdentifier: "showContactInfoSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedContact = filteredContacts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactInfoCell", for: indexPath) as! ContactInfoCell
        cell.configureView(otherContact: selectedContact)
        return cell
    }
    
}
