//
//  ContactInfoCell.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ContactInfoCell: UITableViewCell {

    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func configureView(otherContact: OtherContacts){
        contactName.text = otherContact.contactName
        contactEmail.text = otherContact.contactEmail
    }
    

}
