//
//  ContactInfoVC.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit


protocol ContactInfoChangerDelegate: class{
    func updateData()
}

enum ContactInfoMode{
    case add
    case update
}

class ContactInfoVC: UITableViewController {
    
    @IBOutlet weak var contactName: UITextField!
    @IBOutlet weak var contactEmail: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    fileprivate let firebaseService = FirebaseService()
    
    var otherContact : OtherContacts?
    var contactInfoMode : ContactInfoMode = .add
    weak var delegate : ContactInfoChangerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView(){
        deleteButton.isHidden = contactInfoMode == .add ? true : false
        contactName.text = contactInfoMode == .add ? "" : otherContact?.contactName
        contactEmail.text = contactInfoMode == .add ? "" : otherContact?.contactEmail
    }
    
    @IBAction func saveContactInfo(_ sender: UIButton) {
        if contactInfoMode == .add{
            guard let contactNameString = contactName.text?.trimmingCharacters(in: .whitespaces) , contactNameString != "", let contactEmailString = contactEmail.text?.trimmingCharacters(in: .whitespaces) , contactEmailString != "" else{ return }
            let contactToSave = OtherContacts(contactName: contactNameString, contactEmail: contactEmailString)
            firebaseService.add(type: OtherContacts.self, object: contactToSave)
        }else{
            if var otherContact = otherContact{
                otherContact.contactName = contactName.text ?? ""
                otherContact.contactEmail = contactEmail.text ?? ""
                firebaseService.update(object: otherContact)
            }
        }
        delegate?.updateData()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteContact(_ sender: UIButton) {
        if let otherContact = otherContact{
            firebaseService.delete(object: otherContact)
            delegate?.updateData()
            self.navigationController?.popViewController(animated: true)
        }
    }
}


