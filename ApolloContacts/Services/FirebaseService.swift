//
//  FirebaseService.swift
//  ApolloContacts
//
//  Created by Rudy E Matos on 6/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import FirebaseFirestore


protocol FirebaseObjectType{
    
    static var collectionName : String {get}
    var documentRef: DocumentReference? {get}
    init?(data: [String:Any], documentRef: DocumentReference)
    func parse() -> [String:Any]
    
}

struct CarRental: FirebaseObjectType{
    
    var name : String
    var hotl : String?
    var accountNumber: String?
    var contactEmails : [String]?
    var contactPhones: [String]?
    var notes: String?
    
    static var collectionName = "car_rental"
    private(set) var documentRef: DocumentReference?
    
    init?(data: [String:Any], documentRef: DocumentReference){
        guard let name = data["name"] as? String else{
            return nil
        }
        self.name = name
        if let contactEmails = data["contact_email"] as? [String]{
            self.contactEmails = contactEmails
        }
        if let hotl = data["hotl"] as? String{
            self.hotl = hotl
        }
        
        if let accountNumber = data["account_number"] as? String{
            self.accountNumber = accountNumber
        }
        
        if let contactPhones = data["phone_contacts"] as? [String]{
            self.contactPhones = contactPhones
        }
        
        if let notes = data["notes"] as? String{
            self.notes = notes
        }
    }
    
    func parse() -> [String:Any]{
        return ["" : ""]
    }
    
}

struct Team: FirebaseObjectType{
    
    static var collectionName = "teams"
    
    var name : String
    var notes: String
    var contacts: [String]
    private(set) var documentRef: DocumentReference?
    
    init?(data: [String:Any], documentRef: DocumentReference){
        guard let name = data["name"] as? String, let notes = data["notes"] as? String, let contacts = data["contacts"] as? [String] else{
            return nil
        }
        self.documentRef = documentRef
        self.name = name
        self.notes = notes
        self.contacts = contacts
    }
    
    func parse() -> [String:Any]{
        return ["" : ""]
    }
    
}

struct Escalation: FirebaseObjectType{
    
    static var collectionName = "escalations"
    var contacts : [Contact]
    private(set) var documentRef: DocumentReference?

    struct Contact{
        var name: String
        var index: Int
        var phone : String?
        var position : String?
        init?(data: [String: Any]){
            guard let name = data["name"] as? String, let index = data["index"] as? Int else{
                return nil
            }
            self.name = name
            self.index = index
            if let position = data ["position"] as? String{
                self.position = position
            }
            if let phone = data["phone"] as? String{
                self.phone = phone
            }
        }
    }
    
    init?(data: [String:Any], documentRef: DocumentReference){
        guard let contacts = data["contacts"] as? [[String:Any]] else{
         return nil
        }
        self.documentRef = documentRef
        self.contacts = contacts.compactMap({Contact.init(data: $0)})
    }
    
    func parse() -> [String:Any]{
        return ["" : ""]
    }
    
}

struct OtherContacts: FirebaseObjectType{
    
    private(set) static var collectionName = "other_contacts"
    var contactName: String
    var contactEmail: String
    private(set) var documentRef: DocumentReference?
    
    init(contactName: String, contactEmail: String){
        self.contactName = contactName
        self.contactEmail = contactEmail
    }
    
    init?(data: [String:Any], documentRef : DocumentReference){
        guard let contactName = data["employee_name"] as? String, let contactEmail = data["employee_email"] as? String else{
            return nil
        }
        self.documentRef = documentRef
        self.contactName = contactName
        self.contactEmail = contactEmail
    }
    
    func parse() -> [String:Any]{
        return ["employee_name" : contactName, "employee_email":contactEmail]
    }
    
}

class FirebaseService{
    
    private let db = Firestore.firestore()
    
    func getAll<T : FirebaseObjectType>(type : T.Type, completion: @escaping ([T]?)-> Void){
        db.collection(T.collectionName).getDocuments { (snapshot, error) in
            guard let snapshot = snapshot, error == nil else{
                completion(nil)
                return
            }
            let objects = snapshot.documents.compactMap({T.init(data:$0.data(), documentRef: $0.reference)})
            completion(objects)
        }
    }
    
    
    func update<T: FirebaseObjectType>(object: T){
       object.documentRef?.updateData(object.parse())
    }
    
    func add<T: FirebaseObjectType>(type: T.Type, object: T){
        db.collection(type.collectionName).addDocument(data: object.parse())
    }

    func delete<T: FirebaseObjectType>(object: T){
        object.documentRef?.delete()
    }
    
}
